# Start from golang base image
FROM golang:1.14.6-alpine3.12 as builder

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git

# Copy go mod and sum files 
COPY go.mod go.sum /go/src/gitlab.com/idoko/bucketeer/

# Set the current working directory inside the container 
WORKDIR /go/src/gitlab.com/idoko/bucketeer 

# Download all dependencies. Dependencies will be cached if the go.mod and the go.sum files are not changed 
RUN go mod download 

# Copy the source from the current directory to the working Directory inside the container 
COPY . /go/src/gitlab.com/idoko/bucketeer

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/bucketeer gitlab.com/wfejj/bucketeer/

# Start a new stage from scratch
FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates


# Copy the Pre-built binary file from the previous stage
COPY --from=builder /go/src/gitlab.com/idoko/bucketeer/build/bucketeer /usr/bin/bucketeer



# Expose port 8080 to the outside world
EXPOSE 8080 8080 

ENTRYPOINT ["/usr/bin/bucketeer"]
